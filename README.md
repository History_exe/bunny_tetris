# Bunny Tetris - A game demo for Sony PSP console

## Overview
Bunny Tetris is a simple Tetris clone.

## Authors
- Programming by **Arnaud Storq** (in 2007)
- Graphics by **Made**
- Music by **Maf**

## Video
Video is available on [Youtube](https://youtu.be/8Akd1bq-6gw?t=473).

## Screenshots
![001](docs/SCRSHOT001_MENU.png)
![002](docs/SCRSHOT002_INGAME.png)
![003](docs/SCRSHOT003_GAMEOVER.png)
![004](docs/SCRSHOT004_ENTERNAME.png)
![005](docs/SCRSHOT005_HALLOFFAME.png)
![006](docs/SCRSHOT006_INSTRUCTIONS.png)
