/******************************************************************************
 * display.cpp                                                                *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#include "platform.h"
#include "display.h"

// ---------------------------------------------------------------------------- DEFINE
#define BUF_WIDTH						512
#define SCR_WIDTH						480
#define SCR_HEIGHT						272
#define PIXEL_SIZE						4
#define FRAME_SIZE						BUF_WIDTH * SCR_HEIGHT * PIXEL_SIZE
#define ZBUF_SIZE						( BUF_WIDTH * SCR_HEIGHT * 2 )
#define DEFAULT_BACKGROUND_COLOR		0x00000000

// ---------------------------------------------------------------------------- STATIC MEMBERS
static unsigned int __attribute__( ( aligned( 16 ) ) ) list[ 262144 ];
static void *s_frameBuffer = 0;

// ---------------------------------------------------------------------------- GET DISPLAY WIDTH
int GetDisplayWidth( )
{
	return SCR_WIDTH;
}

// ---------------------------------------------------------------------------- GET DISPLAY HEIGHT
int GetDisplayHeight( )
{
	return SCR_HEIGHT;
}

// ---------------------------------------------------------------------------- GET DISPLAY LIST
unsigned int *GetDisplayList( )
{
	return list;
}

// ---------------------------------------------------------------------------- GET FRAME BUFFER
void *GetFrameBuffer( )
{
	 return ( void * ) ( 0x04000000 + (u32) s_frameBuffer );
}

// ---------------------------------------------------------------------------- UPDATE DISPLAY SYSTEM
void UpdateDisplaySystem( )
{
	s_frameBuffer = sceGuSwapBuffers( );
}

// ---------------------------------------------------------------------------- INIT DISPLAY SYSTEM
void InitDisplaySystem( )
{
	pspDebugScreenPrintf( "initializing display system\n" );
	
	sceGuInit( );

	sceGuStart( GU_DIRECT, list );
	sceGuDrawBuffer( GU_PSM_8888, ( void * ) 0, BUF_WIDTH );
	sceGuDispBuffer( SCR_WIDTH, SCR_HEIGHT, ( void * ) 0x88000, BUF_WIDTH );
	sceGuDepthBuffer( ( void * ) 0x110000, BUF_WIDTH );
	sceGuOffset( 2048 - ( SCR_WIDTH / 2 ), 2048 - ( SCR_HEIGHT / 2 ) );
	sceGuViewport( 2048, 2048, SCR_WIDTH, SCR_HEIGHT );
	sceGuDepthRange( 0xc350, 0x2710 );
	sceGuScissor( 0, 0, SCR_WIDTH, SCR_HEIGHT );
	sceGuEnable( GU_SCISSOR_TEST );
	sceGuDepthFunc (GU_GEQUAL );
	sceGuEnable( GU_DEPTH_TEST );
	sceGuFrontFace( GU_CW );
	sceGuShadeModel( GU_SMOOTH );
	sceGuEnable( GU_CULL_FACE );
	sceGuEnable( GU_TEXTURE_2D );
	sceGuFinish( );
	sceGuSync( 0, 0 );

	sceDisplayWaitVblankStart( );
	sceGuDisplay( GU_TRUE );
	
	sceGuClearColor( DEFAULT_BACKGROUND_COLOR );
	sceGuClearDepth( 0 );
	sceGuClear( GU_COLOR_BUFFER_BIT | GU_DEPTH_BUFFER_BIT );
	
	sceGuFinish( );
	sceGuSync( 0, 0 );

	UpdateDisplaySystem( );
	
	sceGuClearDepth( 0 );
	sceGuClear( GU_COLOR_BUFFER_BIT | GU_DEPTH_BUFFER_BIT );
	
	sceGuFinish( );
	sceGuSync( 0, 0 );
}

// ---------------------------------------------------------------------------- DESTROY DISPLAY SYSTEM
void DestroyDisplaySystem( )
{
	sceGuTerm( );
}
