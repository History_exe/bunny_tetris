/******************************************************************************
 * display.h                                                                  *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#ifndef _DISPLAY_H__
#define _DISPLAY_H__
 
// ---------------------------------------------------------------------------- DISPLAY SYSTEM
void InitDisplaySystem( );
void DestroyDisplaySystem( );

void UpdateDisplaySystem( );

unsigned int *GetDisplayList( );
void *GetFrameBuffer( );
int GetDisplayWidth( );
int GetDisplayHeight( );

#endif 
