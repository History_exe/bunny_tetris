/******************************************************************************
 * font.cpp                                                                   *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#include "platform.h"
#include "font.h"
#include "display.h"

// ---------------------------------------------------------------------------- LOAD FONT
Font *LoadFont( char *filename, char *charset, int charWidth, int charHeight )
{
	bool isOK = false;
	
	Font *font = new Font( filename, charset, charWidth, charHeight, isOK );
	if ( isOK == false )
	{
		delete font;
		font = 0;
	}
	
	return font;
}

// ---------------------------------------------------------------------------- DISPLAY FONT
void DisplayFont( Font *font, char *text, int x, int y )
{	
	int size = strlen( text );
	
	Character character;
	bool isOK;
	
	for ( int iCharacter = 0; iCharacter < size; iCharacter++ )
	{
		character.character = text[ iCharacter ];
		
		font->getCharacter( character, isOK );
		
		if ( isOK == false )
		{
			if ( ( character.character >= 'A' ) && ( character.character <= 'Z' ) )
			{
				character.character += 'a' - 'A';
				
				font->getCharacter( character, isOK );
				if ( isOK == false )
				{
					character.character = ' ';
					font->getCharacter( character, isOK );
				}
			}
			else
			{
				if ( ( character.character >= 'a' ) && ( character.character <= 'z' ) )
				{
					character.character -= 'a' - 'A';
				
					font->getCharacter( character, isOK );
					if ( isOK == false )
					{
						character.character = ' ';
						font->getCharacter( character, isOK );
					}
				}
			}
		}
		
		if ( isOK == true )
		{
			if ( y >= 0 )
			{
				if ( y >= ( GetDisplayHeight( ) - character.height ) )
				{
					character.height -= character.height - ( GetDisplayHeight( ) - y );
				}
				
				if ( ( x > -character.width ) && ( x < 0 ) )
				{
					DisplayImage( character.image, 0, y, character.x - x, character.y, character.width - x, character.height );
				}
				else
				{
					if ( ( x >= 0 ) && ( x < ( GetDisplayWidth( ) /* - character.width */ ) ) )
					{
						DisplayImage( character.image, x, y, character.x, character.y, character.width, character.height );
					}
				}
			}
		}
		
		x += character.width;
	}
}

// ---------------------------------------------------------------------------- GET TEXT WIDTH
int GetTextWidth( Font *font, char *text )
{
	Character character;
	bool isOK;
	
	character.character = ' ';
	font->getCharacter( character, isOK );
	
	return strlen( text ) * character.width;
}

// ---------------------------------------------------------------------------- CONSTRUCTOR
Font::Font( char *filename, char *charset, int charWidth, int charHeight, bool &isOK )
{
	m_image = LoadImage( filename );
	if ( m_image == 0 )
	{
		isOK = false;
		return;
	}
	
	m_charset = ( char * ) memalign( 64, strlen( charset ) + 1 );
	strcpy( m_charset, charset );
	
	m_charWidth = charWidth;
	m_charHeight = charHeight;
	
	isOK = true;
}

// ---------------------------------------------------------------------------- DESTRUCTOR
Font::~Font( )
{
	if ( m_image )
	{
		delete m_image;
	}
	
	if ( m_charset )
	{
		free( m_charset );
	}
}

// ---------------------------------------------------------------------------- GET CHARACTER
void Font::getCharacter( Character &character, bool &isOK )
{
	int x = 0;
	int y = 0;
			
	for ( int iChar = 0; iChar < (int) strlen( m_charset ); iChar++ )
	{
		if ( m_charset[ iChar ] == character.character )
		{
			character.x = x;
			character.y = y;
			character.width = m_charWidth;
			character.height = m_charHeight;
			character.image = m_image;
	
			isOK = true;
			return;
		}
		
		x += m_charWidth;
		if ( x >= m_image->getWidth( ) )
		{
			x = 0;
			y += m_charHeight;
		}
	}
	
	character.x = 0;
	character.y = 0;
	character.width = 0;
	character.height = 0;
	character.image = 0;
	
	isOK = false;
}
