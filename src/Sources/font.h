/******************************************************************************
 * font.h                                                                     *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#ifndef _FONT_H__
#define _FONT_H__

// ---------------------------------------------------------------------------- INCLUDE
#include "image.h"

// ---------------------------------------------------------------------------- CHARACTER CLASS
struct Character
{
	char character;
	Image *image;
	
	int x;
	int y;
	int width;
	int height;
};

// ---------------------------------------------------------------------------- FONT CLASS
class Font
{
// ---------------------------------------------------------------------------- MEMBERS
protected:
	Image *m_image;
	char *m_charset;
	int m_charWidth;
	int m_charHeight;
	
public:

// ---------------------------------------------------------------------------- METHODS
protected:
	
public:
	Font( char *filename, char *charset, int charWidth, int charHeight, bool &isOK );
	~Font( );
	
	Image *getImage( ) { return m_image; };
	char *getCharset( ) { return m_charset; };
	
	void getCharacter( Character &character, bool &isOK );
};

// ---------------------------------------------------------------------------- GLOBAL METHODS
Font *LoadFont( char *filename, char *charset, int charWidth, int charHeight );
void DisplayFont( Font *font, char *text, int x, int y );
int GetTextWidth( Font *font, char *text );

#endif 
