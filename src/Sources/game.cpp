/******************************************************************************
 * game.cpp                                                                   *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#include "platform.h"
#include "game.h"

//---------------------------------------------------------------------------- PARAMETERS
#define NB_ITEMS				    7

#define WAIT_LEVEL_SUIVANT	        1000
#define WAIT_MOVE_DOWN_ITEM         40

#define SCORE_NOUVELLE_ITEM	        1
#define SCORE_1_LINE			    10
#define SCORE_2_LINE			    30
#define SCORE_3_LINE			    80
#define SCORE_4_LINE			    200

#define CALCULATE_WAIT(a)												    \
(																		    \
    (int) ( WAIT_MOVE_DOWN_ITEM - WAIT_MOVE_DOWN_ITEM * 0.1f * (float) a )	\
)

//---------------------------------------------------------------------------- DISPLAY
typedef enum
{
	REMOVE,
	DISPLAY
} Display;

//---------------------------------------------------------------------------- GAME
static char *gameTable = 0;
static int gameWidth = 0;
static int gameHeight = 0;

//---------------------------------------------------------------------------- ITEMS
// Items format :
// the first line is the 4 X,Y sprite of an item
// the second line is the move to do for each rotations
static char itemTable[ ] = 
{
	/* "T" */
    0, -1, 0, 0, 0, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0,

    /* "L" (to the left) */
    0,-1 , 1, -1, 1, 0, 1, 1,
    0, 0, 0, 0, 0, 1, 0, 1,

    /* "L" (to the right) */
    0, -1, -1, -1, -1, 0, -1, 1,
    0, 0, 0, 0, 0, -1, 0, -1,

    /* square */
    0, 0, 0, -1, -1, -1, -1, 0,
    0, 1, -1, 1, -1, 0, 0, 0,

    /* "I" */
    0, -2, 0, -1, 0, 0, 0, 1,
    0, 0, -1, 0, 0, -1, 0, 0,

    /* "Z" (to the left) */
    -1, -1, -1, 0, 0, 0, 0, 1,
    0, 0, 0, 0, -1, 0, 0, -1,

    /* "Z" (to the right) */
    0, -1, 0, 0, -1, 0, -1, 1,
    0, 0, 0, 0, -1, 0, 0, -1
};

static int itemCurrent = 0;
static int itemRotate = 3; // current rotation (from 0 to 3, meaning 0, 90, 180 or 270 degrees)
static int itemPositionX = 0;
static int itemPositionY = 0;

static int minX = 0;
static int maxX = 0;
static int minY = 0;
static int maxY = 0;

static int rotX = 0;
static int rotY = 0;

static int item = 0;

//---------------------------------------------------------------------------- CONTEXT
static int scoreCombo[ ]=
{
	SCORE_1_LINE,
	SCORE_2_LINE,
	SCORE_3_LINE,
	SCORE_4_LINE
};

static int level = 0;
static int score = 0;

static int counterWaitLevel = 0;
static int counterWaitDelay = 0;
static int counterWaitCurrent = 0;

static GameKey keyPress = GameKeyNothing;
static bool end = false;

//---------------------------------------------------------------------------- GAME MANAGEMENT
static void gameClear(void)
{
	memset( gameTable, 0, gameWidth * gameHeight );
}

static void gameUpdateValue( int x, int y, int value )
{
	gameTable[ x + y * gameWidth ] = value;
}

static char gameGetValue( int x, int y )
{
	return gameTable[ x + y * gameWidth ];
}

static void gameRemoveLines(void)
{
	int x = 0;
	int y = 0;

	int counterLine;

	int combo = 0;

	// for each lines of the table
    for ( y = 0; y < gameHeight ; y++ )
	{
		counterLine = 0;
		
		// for each horizontal blocks
        for ( x = 0; x < gameWidth; x++ )
		{
			if ( gameGetValue( x, y ) != 0 )
			{
				counterLine++;
			}
		}

		// if the line is full
        if ( counterLine == gameWidth )
		{
			// move to the down the above content
            while ( y > 0 )
			{
				for ( x = 0; x < gameWidth; x++ )
				{
					gameUpdateValue( x, y, gameGetValue( x, y - 1 ) );
				}

				y--;
			}

			// clear the first line
            for ( x = 0; x < gameWidth; x++ )
			{
				gameUpdateValue( x, 0, 0 );
			}

			y = 0;

			combo++;
		}
	}

	// manage score 
    if ( combo > 0 )
	{
		score += scoreCombo[ combo - 1 ];
	}
}

//---------------------------------------------------------------------------- ITEMS MANAGEMENT
static void itemCalculateRotate( int item, int carreIndex, int rotation )
{
	int itemIndex = 0;
	int rotationIndex = 0;

	int swap = 0;

	itemIndex = item * 16;

	// Get current block
    // (between 0 and 3, because an item is made of 4 blocks)
    rotX = itemTable[ itemIndex + carreIndex * 2 ];
	rotY = itemTable[ itemIndex + carreIndex * 2 + 1 ];

	// Inverse the position of the blocks in function of current rotation
    switch ( rotation )
	{
	case 0:
		break;

	case 1:
		swap = rotY;
		rotY = rotX;
		rotX = -swap;
		break;

	case 2:
		rotX = -rotX;
		rotY = -rotY;
		break;

	case 3:
		swap = rotY;
		rotY = -rotX;
		rotX = swap;
		break;
	}

	rotationIndex = rotation * 2 + 8;

	rotX += itemTable[ itemIndex + rotationIndex ];
	rotY += itemTable[ itemIndex + rotationIndex + 1 ];
}

static void itemGetMinMax( int item, int rotate )
{
	int i = 0;
	
	minX = 100;
	maxX = -100;
	minY = 100;
	maxY = -100;

	// for each blocks
    for ( i = 0; i < 4; i++ )
	{
		// get current rotation for current item
        itemCalculateRotate( item, i, rotate );

		// compare to previous values
        if ( rotX < minX ) minX = rotX;
		if ( rotY < minY ) minY = rotY;
		if ( rotX < maxX ) maxX = rotX;
		if ( rotY < maxY ) maxY = rotY;
	}
}

// check if the item can be inserted at new position
static bool itemCanMove( )
{
	int i = 0;

	int x = 0;
	int y = 0;
	
	// for each blocks of the current item
    for ( i = 0; i < 4; i++ )
	{
		itemCalculateRotate( itemCurrent, i, itemRotate );

		x = rotX + itemPositionX;
		y = rotY + itemPositionY;

		// check if the item is out of the table
        if ( ( x < 0 ) || ( x >= gameWidth ) || ( y >= gameHeight ) )
		{
			return false;
		}
		// check if the item is vertically out the table
        // if yes, let's move down the item and let's start again
		else if ( y < 0 )
		{
			itemPositionY++;

			i = -1;
		}
		// check if the block isn't already used in the table
        else if ( gameGetValue( x, y ) != 0 )
		{
			return false;
		}
	}

	return true;
}

// get new item
static int itemNext( )
{
	// a customized random !
    item += counterWaitCurrent * counterWaitCurrent + score + level;
	
	// get a value from 0 and 6 (for different 7 items)
    return item % NB_ITEMS;
}

static void itemInitialize( )
{
	// default rotation is 270 degrees
    itemRotate = 3;

	itemGetMinMax( itemCurrent, itemRotate );

	// default position : center of the screen at bottom of the table
    itemPositionX = gameWidth / 2;
	itemPositionY = minY;
}

static void itemInsertNewItem( )
{
	itemCurrent = itemNext( );
	itemInitialize( );

	if ( !itemCanMove( ) )
	{
		end = true;
	}
}

static void itemDisplay( Display display )
{
	int i = 0;

	int x = 0;
	int y = 0;

	int value = 0;

	if ( display == REMOVE )
	{
		value = 0;
	}
	else
	{
		value = 1 + itemCurrent;
	}

	// for each blocks of the item
    for ( i = 0; i < 4; i++ )
	{
		itemCalculateRotate( itemCurrent, i, itemRotate );

		x = rotX + itemPositionX;
		y = rotY + itemPositionY;

		if ( ( x >= 0 ) && ( x < gameWidth ) && ( y >= 0 ) && ( y < gameHeight ) )
		{	
			gameUpdateValue( x, y, value );
		}
	}
}

//---------------------------------------------------------------------------- MOVE MANAGEMENT
static void moveX( int a )
{
	itemPositionX += a;

	if ( !itemCanMove( ) )
	{
		itemPositionX -= a;
	}
}

static void moveDown(void)
{
	itemPositionY++;
		
	if ( !itemCanMove( ) )
	{
		itemPositionY--;
		
		itemDisplay( DISPLAY );

		gameRemoveLines( );

		score += level;
		itemInsertNewItem( );
	}
}

static void rotate( )
{
	itemRotate++;
	itemRotate %= 4;

	if ( !itemCanMove( ) )
	{
		itemRotate--;
		itemRotate %= 4;
	}
}
//---------------------------------------------------------------------------- CONTEXT MANAGEMENT
static void gameNew( )
{
	level = 1;
	score = 0;

	counterWaitDelay = CALCULATE_WAIT( level );
	counterWaitLevel = WAIT_LEVEL_SUIVANT;
	counterWaitCurrent = counterWaitDelay;

	end = false;

	gameClear( );
}

//---------------------------------------------------------------------------- INTERFACES
void GameCreate( int largeur, int hauteur )
{
	gameWidth = largeur;
	gameHeight = hauteur;
	
	gameTable = new char[ gameWidth * gameHeight ];

	gameNew( );
	
	itemCurrent = itemNext( );
	itemInitialize( );
}

void GameUpdate( )
{
	if (end==true)
	{
		gameNew( );
	}
		
	itemDisplay( REMOVE );

	switch( keyPress )
	{
	case GameKeyRotate:
		rotate( );
		break;

	case GameKeyLeft:
		moveX( -1 );		
		break;

	case GameKeyRight:
		moveX( 1 );
		break;

	case GameKeyDown:
		moveDown( );		
		break;
	}

	keyPress = GameKeyNothing;

	counterWaitCurrent--;
	if ( counterWaitCurrent < 0 )
	{
		counterWaitCurrent = counterWaitDelay;
		
		moveDown( );
	}

	if ( !end )
	{
		itemDisplay( DISPLAY );

		counterWaitLevel--;
		if (counterWaitLevel < 0)
		{
			counterWaitLevel = WAIT_LEVEL_SUIVANT;

			level++;
			counterWaitDelay = CALCULATE_WAIT( level );
		}
	}
}

void GameRelease( )
{
	delete [] gameTable;
}

void GameKeyPress( GameKey key )
{
	if ( key!=GameKeyNothing )
	{
		keyPress = key;
	}
}

void GameGetContext( GameContext *context )
{
	if ( context )
	{
		context->gameTable = gameTable;
		context->gameWidth = gameWidth;
		context->gameHeight = gameHeight;

		context->level = level;
		context->score = score;

		context->end = end;
	}
}
