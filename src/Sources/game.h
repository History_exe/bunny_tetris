/******************************************************************************
 * game.h                                                                     *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#ifndef _GAME_H__
#define _GAME_H__

//---------------------------------------------------------------------------- CONTEXT
typedef struct GameContext
{
	char *gameTable;
	int gameWidth;
	int gameHeight;

	int score;
	int level;

	bool end;
} GameContext;

//---------------------------------------------------------------------------- GAME KEYS
typedef enum
{
	GameKeyNothing,
	GameKeyLeft,
    GameKeyRight,
	GameKeyRotate,
    GameKeyDown
} GameKey;

//---------------------------------------------------------------------------- GLOBAL FUNCTIONS
void GameCreate( int width, int height );
void GameUpdate( );
void GameKeyPress( GameKey key );
void GameRelease( );

void GameGetContext( GameContext *context );

//---------------------------------------------------------------------------- END
#endif // _GAME_H__
