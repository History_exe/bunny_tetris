/******************************************************************************
 * image.cpp                                                                  *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#include "platform.h"
#include "image.h"
#include "display.h"
#include "libtarga.h"

// ---------------------------------------------------------------------------- LOAD IMAGE
Image *LoadImage( char *filename )
{
	bool isOK = false;
	
	Image *image = new Image( filename, isOK );
	if ( isOK == false )
	{
		delete image;
		image = 0;
	}
	
	char debugText[ 256 ];
	strcpy( debugText, filename );
	if ( isOK == true )
	{
		strcat( debugText, " loaded\n" );
		
	}
	else
	{
		strcat( debugText, " failed !!!\n" );
	}
	pspDebugScreenPrintf( debugText );
	if ( isOK == false )
	{
		for ( ; ; );
	}
	
	return image;
}

// ---------------------------------------------------------------------------- DISPLAY IMAGE
void DisplayImage( Image *image, int x, int y )
{
	sceGuCopyImage( GU_PSM_8888, 0, 0, image->getWidth( ), image->getHeight( ), image->getWidthPower2( ), image->getData( ), x, y, 512, GetFrameBuffer( ) );
}

// ---------------------------------------------------------------------------- DISPLAY IMAGE
void DisplayImage( Image *image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight )
{
	sceGuCopyImage( GU_PSM_8888, srcX, srcY, srcWidth, srcHeight, image->getWidthPower2( ), image->getData( ), x, y, 512, GetFrameBuffer( ) );
}

// ---------------------------------------------------------------------------- CONSTRUCTOR
Image::Image( char *filename, bool &isOK )
{
	m_data = 0;
	m_width = 0;
	m_height = 0;
	m_widthPower2 = 0;
	m_heightPower2 = 0;
	
	SceIoStat fileStatus;
	if ( sceIoGetstat( filename, &fileStatus ) < 0 )
	{
		isOK = false;
		return;
	}
	
	void *tgaData = memalign( 64, fileStatus.st_size );
	if ( tgaData == 0 )
	{
		m_width = 0;
		m_height = 0;
	
		isOK = false;
		return;
	}

	SceUID handle = sceIoOpen( filename, PSP_O_RDONLY, 0777 );
	if ( handle )
	{
		sceIoRead( handle, tgaData, fileStatus.st_size );
		sceIoClose( handle );
	}
	else
	{
		m_width = 0;
		m_height = 0;
	
		isOK = false;
		return;
	}
	
	m_data = tga_load( tgaData, fileStatus.st_size, &m_width, &m_height, TGA_TRUECOLOR_32 );
	if ( m_data == 0 )
	{
		m_width = 0;
		m_height = 0;
	
		free( tgaData );
	
		isOK = false;
		return;
	}
	
	free( tgaData );
	
	flip( );
	scalePower2( );
        
	isOK = true;
}

// ---------------------------------------------------------------------------- DESTRUCTOR
Image::~Image( )
{
	if ( m_data )
	{
		free( m_data ); // do not use delete here, as libtarga is using malloc
	}
}

// ---------------------------------------------------------------------------- FLIP
void Image::flip( )
{
   for ( int y = 0; y < ( m_height / 2 ); ++y )
	{ 
        int offsetY = ( m_width * ( m_height - 1 ) );

		for ( int x = 0; x < m_width; ++x )
		{
			int yScanline = m_width * y;

			unsigned int texel = ( ( unsigned int * ) m_data)[ yScanline + x ];

			( ( unsigned int * ) m_data)[ yScanline + x ] = ( ( unsigned int * ) m_data)[ offsetY - yScanline + x ];

			( ( unsigned int * ) m_data)[ offsetY - yScanline + x ] = texel;
		}
	}
}

// ---------------------------------------------------------------------------- GET POWER 2
int Image::getPower2( int value )
{
	int bit = 0;
	int lastBit = 0;

	for ( int i = 0; i < 32; ++i )
	{
		int bitCmp = 1 << i;

		if ( value & bitCmp )
		{
			bit++;
			lastBit = bitCmp;
		}
	}

	if ( bit != 1 )
	{
		value = lastBit << 1;
	}
	
	return value;
}

// ---------------------------------------------------------------------------- SCALE POWER 2
void Image::scalePower2( )
{
    int newWidth = getPower2( m_width );
	int newHeight = getPower2( m_height );

	if ( newWidth > newHeight )
	{
		newHeight = newWidth;
	}
	else
	{
		newWidth = newHeight;
	}

	if ( ( newWidth != m_width ) || ( newHeight != m_height ) )
	{
		unsigned int *data = ( unsigned int * ) memalign( 64, sizeof( unsigned int ) * newWidth * newHeight );

		/*for ( int y = 0; y < newHeight; ++y )
		{
			for ( int x = 0; x < newWidth; ++x )
			{
				float srcX = ( ( (float) x ) / ( (float) newWidth ) ) * ( (float) m_width );
				float srcY = ( ( (float) y ) / ( (float) newHeight ) ) * ( (float) m_height );

				int srcOffset = ( ( (int) srcY ) * m_width ) + ( (int) srcX );
				
				data[ ( y * newWidth ) + x ] = ( ( unsigned int * ) m_data )[ srcOffset ];
			}
		}*/
		
		for ( int y = 0; y < m_height; ++y )
		{
			for ( int x = 0; x < m_width; ++x )
			{
				int srcOffset = ( ( (int) y ) * m_width ) + ( (int) x );
				
				data[ ( y * newWidth ) + x ] = ( ( unsigned int * ) m_data )[ srcOffset ];
			}
		}
		
    
		free( m_data );
		m_data = data;
		
		m_widthPower2 = newWidth;
		m_heightPower2 = newHeight;
	}
}
