/******************************************************************************
 * image.h                                                                    *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#ifndef _IMAGE_H__
#define _IMAGE_H__

// ---------------------------------------------------------------------------- IMAGE CLASS
class Image
{
// ---------------------------------------------------------------------------- MEMBERS
protected:
	void *m_data;
	int m_width;
	int m_height;
	int m_widthPower2;
	int m_heightPower2;
	
public:

// ---------------------------------------------------------------------------- METHODS
protected:
	void scalePower2( );
	int getPower2( int value );
	void flip( );
	
public:
	Image( char *filename, bool &isOK );
	~Image( );
	
	int getWidth( ) { return m_width; };
	int getHeight( ) { return m_height; };
	int getWidthPower2( ) { return m_widthPower2; };
	int getHeightPower2( ) { return m_heightPower2; };
	void *getData( ) { return m_data; };
};

// ---------------------------------------------------------------------------- GLOBAL METHODS
Image *LoadImage( char *filename );
void DisplayImage( Image *image, int x, int y );
void DisplayImage( Image *image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight );

#endif 
