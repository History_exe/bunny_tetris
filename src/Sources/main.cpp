/******************************************************************************
 * main.cpp                                                                   *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#include "platform.h"
#include "sound.h"
#include "display.h"
#include "image.h"
#include "font.h"
#include "game.h"

// ---------------------------------------------------------------------------- PSP INFO
PSP_MODULE_INFO( APP_NAME, 0, 1, 1 );
PSP_MAIN_THREAD_ATTR( THREAD_ATTR_USER | THREAD_ATTR_VFPU );

// ---------------------------------------------------------------------------- MENU DEFINE
#define TETRIS_WIDTH				12
#define TETRIS_HEIGHT				18
#define TETRIS_POSX					163
#define TETRIS_POSY					37
#define KEYS_UPDATE_TIME			4
#define KEYS_UPDATE_SLOW_TIME		7
#define KEYS_UPDATE_VERY_SLOW_TIME	20
#define SONG_COUNT					4
#define IMAGE_COUNT					6
#define DISPLAY_ABOUT_TIME			1660

// ---------------------------------------------------------------------------- STATIC
static Font *s_scrollFont = 0;
static Font *s_writerFont = 0;
static Song *s_songs[ SONG_COUNT ];
static int s_currentSong = 0;
static Image *s_gameBG_1 = 0;
static Image *s_gameBG_2 = 0;
static Image *s_gameBG_3 = 0;
static char *s_menuScrollText = "WELCOME TO *BUNNY TETRIS* !         INSTRUCTIONS: USE #LEFT# AND #RIGHT# BUTTONS TO MOVE THE PIECE, #DOWN# BUTTON TO ACCELERATE, #CROSS# OR #CIRCLE# BUTTONS TO ROTATE, #SELECT# BUTTON TO CHANGE MUSIC THEME !         LAME WIKIPEDIA COPY/PASTE: TETROMINOES OR TETRADS, SHAPES COMPOSED OF FOUR BLOCKS EACH, FALL DOWN A WELL AND THE PLAYER HAS TO ROTATE AND/OR MOVE THEM WITH THE AIM OF CREATING A HORIZONTAL LINE OF BLOCKS WITH NO GAPS. WHEN SUCH A LINE IS CREATED, IT DISAPPEARS, AND THE PIECES ABOVE (IF ANY) FALL.         CREDITS: GAME CODE BY ARNAUD STORQ AKA *NO RECESS*, MUSIC BY BENOIT CHARCOSSET AKA *MAF*, GFX BY CARLOS PARDO AKA *MADE*.         I HOPE YOU ENJOY THIS GAME, AS THIS REPRESENTS MY REALLY FIRST PSP PRODUCTION ! BTW - DID YOU PRESS #START# BUTTON, HEHE ? :)         GREETINGS : THE WHOLE *PSPDEV* TEAM (WITHOUT YOU, THIS PRODUCTION WON'T BE AVAILABLE !), *ADRESD*, *TYRANID*, *MR-SPIV* (THANKS FOR THE GREAT OSX PSPSDK PORT !), *YOYO*, *NIPPY*..         IF YOU ENJOY THIS PRODUCTION, DROP ME A LINE IN MY GUESTBOOK ! HTTP://NORECESS.PLANET-D.NET         TIME TO GO !         *NO RECESS* - SEPTEMBER 5, 2005.                           ";
static int s_menuScrollX = 0;
static Font *s_gamePieces = 0;
static int keysUpdateTime = 0;
static Image *s_aboutBG = 0;
static Image *s_aboutIMG[ IMAGE_COUNT ];
static int s_displayAboutTime = -1;
static int s_aboutCurrentImage = 0;
static int s_aboutImagesX = 0;
static char s_appPath[ 512 ];
static char s_filename[ 512 ];

// ---------------------------------------------------------------------------- MAKE RELATIVE PATH
char *makeRelativePath( char *filename )
{
	strcpy( s_filename, s_appPath );
	
	char *p = strrchr( s_filename, '/' ) + 1;
	
	strcpy( p, filename );
	
	return s_filename;
}

// ---------------------------------------------------------------------------- ABOUT PRE-INIT
void aboutPreInit( )
{
	s_aboutBG = LoadImage( makeRelativePath( "BG_About.tga" ) );
	
	s_aboutIMG[ 0 ] = LoadImage( makeRelativePath( "IMG_1.tga" ) );
	s_aboutIMG[ 1 ] = LoadImage( makeRelativePath( "IMG_2.tga" ) );
	s_aboutIMG[ 2 ] = LoadImage( makeRelativePath( "IMG_3.tga" ) );
	s_aboutIMG[ 3 ] = LoadImage( makeRelativePath( "IMG_4.tga" ) );
	s_aboutIMG[ 4 ] = LoadImage( makeRelativePath( "IMG_5.tga" ) );
	s_aboutIMG[ 5 ] = LoadImage( makeRelativePath( "IMG_6.tga" ) );
}

// ---------------------------------------------------------------------------- ABOUT INIT
void aboutInit( )
{
	s_aboutCurrentImage	= -1;
	
	s_aboutImagesX = 0;
}

// ---------------------------------------------------------------------------- ABOUT UPDATE
void aboutUpdate( )
{
	sceGuStart( GU_DIRECT, GetDisplayList( ) );
		
	if ( ( s_displayAboutTime % ( DISPLAY_ABOUT_TIME / 8 ) ) > ( DISPLAY_ABOUT_TIME / 64 ) )
	{
		s_aboutImagesX -= 6;
	
		int x = s_aboutImagesX;
		int y = 12;
			
		for ( int i = 0; i < IMAGE_COUNT * 7; i++ )
		{
			int iImage = i % ( IMAGE_COUNT );
			
			int width = s_aboutIMG[ iImage ]->getWidth( );
	
			if ( ( x > -width ) && ( x < 0 ) )
			{
				DisplayImage(  s_aboutIMG[ iImage ], 0, y, - x, 0, width + x, s_aboutIMG[ iImage ]->getHeight( ) );
			}
			else
			{
				if ( ( x >= ( GetDisplayWidth( ) - width ) ) && ( x < GetDisplayWidth( ) ) )
				{
					DisplayImage( s_aboutIMG[ iImage ], x, y, 0, 0, GetDisplayWidth( ) - x, s_aboutIMG[ iImage ]->getHeight( ) );
				}
				else
				{
					if ( ( x >= 0 ) && ( x < ( GetDisplayWidth( ) - width ) ) )
					{
						DisplayImage( s_aboutIMG[ iImage ], x, y );
					}
				}
			}
				
			x += s_aboutIMG[ iImage ]->getWidth( );
		}
	}
	else
	{
		DisplayImage( s_aboutBG, 0, 0 );
	}
		
	sceGuFinish( );	
	sceGuSync( 0, 0 );
	
	sceDisplayWaitVblankStart( );
	UpdateDisplaySystem( );
}

// ---------------------------------------------------------------------------- ABOUT POST-DESTROY
void aboutPostDestroy( )
{
	for ( int iImage = 0; iImage < IMAGE_COUNT; iImage++ )
	{
		delete s_aboutIMG[ iImage ];
	}
	
	delete s_aboutBG;
}

// ---------------------------------------------------------------------------- NEXT SONG
void nextSong( )
{
	s_currentSong++;
	if ( s_currentSong >= ( SONG_COUNT - 1 ) ) // -1 because last one is the About one
	{
		s_currentSong = 0;
	}
	
	PlaySong( s_songs[ s_currentSong ] );
}

// ---------------------------------------------------------------------------- KEYS UPDATE
void keysUpdate( )
{
	keysUpdateTime--;
	if ( keysUpdateTime < 0 )
	{
		bool slowKeyPushed = false;
		bool verySlowKeyPushed = false;
		
		SceCtrlData pad;
		sceCtrlReadBufferPositive( &pad, 1 );
	
		if ( s_displayAboutTime == -1 )
		{
			if ( ( pad.Buttons & PSP_CTRL_LEFT ) || ( pad.Buttons & PSP_CTRL_LTRIGGER ) )
			{
				GameKeyPress( GameKeyLeft );
			}
	
			if ( ( pad.Buttons & PSP_CTRL_RIGHT ) || ( pad.Buttons & PSP_CTRL_RTRIGGER ) )
			{
				GameKeyPress( GameKeyRight );
			}
	
			if ( pad.Buttons & PSP_CTRL_DOWN )
			{
				GameKeyPress( GameKeyDown );
			}
	
			if ( ( pad.Buttons & PSP_CTRL_CROSS ) || ( pad.Buttons & PSP_CTRL_CIRCLE ) || ( pad.Buttons & PSP_CTRL_SQUARE ) || ( pad.Buttons & PSP_CTRL_TRIANGLE ) )
			{
				GameKeyPress( GameKeyRotate );
				slowKeyPushed = true;
			}
		
			if ( pad.Buttons & PSP_CTRL_SELECT )
			{
				nextSong( );
			
				verySlowKeyPushed = true;
			}
		}
		
		if ( pad.Buttons & PSP_CTRL_START )
		{
			if ( s_displayAboutTime >= 0 )
			{
				nextSong( );
				
				s_displayAboutTime = -1;
			}
			else
			{
				s_displayAboutTime = DISPLAY_ABOUT_TIME;
			
				aboutInit( );
			
				PlaySong( s_songs[ SONG_COUNT - 1 ] );
			}
			
			verySlowKeyPushed = true;
		}
		
		if ( verySlowKeyPushed == true )
		{
			keysUpdateTime = KEYS_UPDATE_VERY_SLOW_TIME;
		}
		else
		if ( slowKeyPushed == true )
		{
			keysUpdateTime = KEYS_UPDATE_SLOW_TIME;
		}
		else
		{
			keysUpdateTime = KEYS_UPDATE_TIME;
		}
	}
}

// ---------------------------------------------------------------------------- MENU PRE-INIT
void menuPreInit( )
{
	s_gameBG_1 = LoadImage( makeRelativePath( "BG_Bunny1.tga" ) );
	s_gameBG_2 = LoadImage( makeRelativePath( "BG_Bunny2.tga" ) );
	s_gameBG_3 = LoadImage( makeRelativePath( "BG_Bunny3.tga" ) );
	
	s_gamePieces = LoadFont( makeRelativePath( "GAME_Pieces.tga" ), "1234567", 13, 13 );
}

// ---------------------------------------------------------------------------- MENU INIT
void menuInit( )
{
	s_menuScrollX = GetDisplayWidth( );
	keysUpdateTime = 0;
	s_displayAboutTime = -1;
	
	GameCreate( TETRIS_WIDTH, TETRIS_HEIGHT );
}

// ---------------------------------------------------------------------------- MENU DESTROY
void menuDestroy( )
{
	GameRelease( );
}

// ---------------------------------------------------------------------------- MENU POST-DESTROY
void menuPostDestroy( )
{
	delete s_gameBG_1;
	delete s_gameBG_2;
	delete s_gameBG_3;
	
	delete s_gamePieces;
}

// ---------------------------------------------------------------------------- INIT GAME
void InitAll( )
{
	pspDebugScreenInit( );
	pspDebugScreenClear( );
	pspDebugScreenSetXY( 0, 0 );
	pspDebugScreenSetTextColor( 0xFFFFFFFF );
	pspDebugScreenPrintf( "BUNNY TETRIS v1.0\n");
	pspDebugScreenPrintf( "game code by Arnaud STORQ (http://norecess.planet-d.net)\n" );
	pspDebugScreenPrintf( "compiled using PSPSDK (http://www.pspdev.org)\n");
	pspDebugScreenPrintf( "\n" );
	
	InitSoundSystem( );
	
	s_songs[ 0 ] = LoadSong( makeRelativePath( "now panikk!.mod" ) );
	s_songs[ 1 ] = LoadSong( makeRelativePath( "hear me moving.mod" ) );
	s_songs[ 2 ] = LoadSong( makeRelativePath( "kloon land.mod" ) );
	s_songs[ 3 ] = LoadSong( makeRelativePath( "teu-teu-teu-yeu-teu.mod" ) );
	
	s_scrollFont = LoadFont( makeRelativePath( "FNT_16x24.tga" ), "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,:'-#*?!/() ", 16, 24 );	
	s_writerFont = LoadFont( makeRelativePath( "FNT_8x9.tga" ), "ABCDEFGHIJKLMNOPQRSTUVWXYZ!()-'.,123456 7890 ", 8, 10 );	
	
	menuPreInit( );
	aboutPreInit( );
	
	InitDisplaySystem( );
	
	menuInit( );
	
	s_currentSong = SONG_COUNT;
	nextSong( );
}

// ---------------------------------------------------------------------------- DESTROY GAME
void DestroyAll( )
{
	aboutPostDestroy( );
	menuPostDestroy( );
	
	delete s_writerFont;
	delete s_scrollFont;
	
	for ( int iSong = 0; iSong < SONG_COUNT; iSong++ )
	{
		delete s_songs[ iSong ];
	}
	
	DestroySoundSystem( );
	DestroyDisplaySystem( );
}

// ---------------------------------------------------------------------------- UPDATE ALL
void UpdateAll( )
{
	keysUpdate( );
	
	if ( s_displayAboutTime >= 0 )
	{
		aboutUpdate( );
		
		s_displayAboutTime--;
		if ( s_displayAboutTime < 0 )
		{
			s_displayAboutTime = -1;
			
			nextSong( );
		}
		
		return;
	}
	
	GameUpdate( );
	
	GameContext context;
    GameGetContext( &context );
	
	if ( context.end == true )
	{
		nextSong( );
	}

	sceGuStart( GU_DIRECT, GetDisplayList( ) );
		
	Image *backGround = s_gameBG_3;
	for ( int y = TETRIS_HEIGHT - 1; y >= 0; y-- )
	{
		for ( int x = 0; x < TETRIS_WIDTH; x++ )
		{
			if ( context.gameTable[ y * TETRIS_WIDTH + x ] != 0 )
			{
				if ( y < ( TETRIS_HEIGHT / 3 ) )
				{
					backGround = s_gameBG_3;
				}
				else
				if ( y < ( 2 * ( TETRIS_HEIGHT / 3 ) ) )
				{
					backGround = s_gameBG_2;
				}
				else
				{
					backGround = s_gameBG_1;
				}
			}
		}
	}
			
	DisplayImage( backGround, 0, 0 );
		
	s_menuScrollX -= 2;
	if ( s_menuScrollX < -GetTextWidth( s_scrollFont, s_menuScrollText ) )
	{
		s_menuScrollX = GetDisplayWidth( );
	}
				
	DisplayFont( s_scrollFont, s_menuScrollText, s_menuScrollX, 13 );
	
	for ( int y = 0; y < TETRIS_HEIGHT; y++ )
	{
		for ( int x = 0; x < TETRIS_WIDTH; x++ )
		{
			if ( context.gameTable[ y * TETRIS_WIDTH + x ] != 0 )
			{
				char text[ 2 ];
				text[ 0 ] = '0' + context.gameTable[ y * TETRIS_WIDTH + x ];
				text[ 1 ] = 0;
			
				DisplayFont( s_gamePieces, text, TETRIS_POSX + x * 13, TETRIS_POSY + y * 13 );
			}
		}
	}
	
	int value;
	int x;
	DisplayFont( s_writerFont, "SCORE", 323, 37 );
	char number[ 256 ];
	sprintf( number, "%d", context.score );
	x = 323 + 4 * 8;
	value = context.score;
	while ( value >= 10 )
	{
		value /= 10;
		x -= 8;
	}	
	DisplayFont( s_writerFont, number, x, 37 + 8 );
	
	DisplayFont( s_writerFont, "LEVEL", 324, 37 + 8 * 3 );
	sprintf( number, "%d", context.level );
	x = 323 + 4 * 8;
	value = context.level;
	while ( value >= 10 )
	{
		value /= 10;
		x -= 8;
	}	
	DisplayFont( s_writerFont, number, x, 37 + 8 * 4 );		
	
	sceGuFinish( );
	sceGuSync( 0, 0 );
				
	UpdateDisplaySystem( );
	sceDisplayWaitVblankStart( );
}

// ---------------------------------------------------------------------------- MAIN
int main( int argc, char *argv[ ] )
{
	SetupCallbacks();

	strcpy( s_appPath, argv[ 0 ] );
	char *p = strrchr( s_appPath, '/' );
	*++p = 0;
	
	InitAll( );
	
	for ( ; ; )
	{
		UpdateAll( );
	}
	
	DestroyAll( );
	
	sceKernelExitGame( );
	
	return 0;
}
