/******************************************************************************
 * platform.cpp                                                               *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#include "platform.h"

// ---------------------------------------------------------------------------- DEFINE
#define SIN_ITERATOR 20

// ---------------------------------------------------------------------------- AVOID LINK PROBLEM
int __errno = 0;

extern void DestroyAll( );

// ---------------------------------------------------------------------------- CALLBACK EXIT
int CallbackExit( int arg1, int arg2, void *common )
{
	DestroyAll( );
	
	sceKernelExitGame( );
	return 0;
}

// ---------------------------------------------------------------------------- CALLBACK THREAD
int CallbackThread( SceSize args, void *argp )
{
	int cbid = sceKernelCreateCallback( "Exit Callback", CallbackExit, NULL );
	sceKernelRegisterExitCallback( cbid );

	sceKernelSleepThreadCB( );

	return 0;
}

// ---------------------------------------------------------------------------- SETUP CALLBACKS
int SetupCallbacks( void )
{
	int thid = sceKernelCreateThread( "update_thread", CallbackThread, 0x11, 0xFA0, 0, 0 );
	if (thid >= 0)
	{
		sceKernelStartThread( thid, 0, 0 );
	}

	return thid;
}

// ---------------------------------------------------------------------------- SINF
float sinf( float value )
{
	float res;
	float w;
	int t;
	float fac;
	
	int i = ( int ) ( ( value ) / ( 2.0f * MATH_PI ) );
	value -= i * 2.0f * MATH_PI;

	fac = 1.0f;
	res = 0.0f;
	w = value;
	
	for ( t = 1; t < SIN_ITERATOR; )
	{
		res += fac * w;
		w *= value * value;
		t++;
		fac /= t;
		t++;
		fac /= t;

		res -= fac * w;
		w *= value * value;
		t++;
		fac /= t;
		t++;
		fac /= t;
	}
	
	return res;
}

// ---------------------------------------------------------------------------- COSF
float cosf( float value )
{
	float res;
	float w;
	int t;
	float fac;
	
	int i = ( int ) ( ( value ) / ( 2.0f * MATH_PI ) );
	value -= i * 2.0f * MATH_PI;

	fac = 1.0f;
	res = 0.0f;
	w = 1.0f;
	for ( t = 0; t < SIN_ITERATOR; )
	{
		res += fac * w;
		w *= value * value;
		t++;
		fac /= t;
		t++;
		fac /= t;

		res -= fac * w;
		w *= value * value;
		t++;
		fac /= t;
		t++;
		fac /= t;
	}
	
	return res;
}
