/******************************************************************************
 * platform.h                                                                 *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#ifndef _PLATFORM_H__
#define _PLATFORM_H__
 
// ---------------------------------------------------------------------------- INCLUDE PSPSDK
#include <pspkernel.h>
#include <pspthreadman.h>
#include <pspiofilemgr.h>
#include <pspiofilemgr_dirent.h> 
#include <pspctrl.h>
#include <pspdisplay.h>
#include <pspdebug.h>
#include <pspctrl.h>
#include <pspdisplay.h>
#include <pspumd.h>

// ---------------------------------------------------------------------------- INCLUDE STD
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <pspgu.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <malloc.h>

// ---------------------------------------------------------------------------- DEFINE
#define APP_NAME	"Bunny Tetris"
#define MATH_PI		3.14159265f

// ---------------------------------------------------------------------------- CALLBACKS
int SetupCallbacks( void );
int CallbackExit( int arg1, int arg2, void *common );
int CallbackThread( SceSize args, void *argp );

float sinf( float value );
float cosf( float value );

#endif 
