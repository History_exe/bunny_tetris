/******************************************************************************
 * sound.cpp                                                                  *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#include "platform.h"
#include "mikmod.h"
#include "sound.h"

// ---------------------------------------------------------------------------- ERROR HANDLER
static void my_error_handler( void )
{
}

// ---------------------------------------------------------------------------- INIT SOUND SYSTEM
void InitSoundSystem( )
{
	_mm_RegisterErrorHandler( my_error_handler );
	
	MikMod_RegisterAllLoaders();
	MikMod_RegisterAllDrivers();

	md_mode = DMODE_16BITS | DMODE_STEREO | DMODE_SOFT_SNDFX | DMODE_SOFT_MUSIC | DMODE_INTERP; 
	MikMod_Init( ) ;
	
	pspDebugScreenPrintf( "mikmod sound system initialized\n" );
}

// ---------------------------------------------------------------------------- DESTROY SOUND SYSTEM
void DestroySoundSystem( )
{
    StopSong( );

	MikMod_Exit( );
}

// ---------------------------------------------------------------------------- LOAD SONG
Song *LoadSong( char *filename )
{
	bool isOK = false;
	
	Song *song = new Song( filename, isOK );
	if ( isOK == false )
	{
		delete song;
		song = 0;
	}
	
	char debugText[ 256 ];
	strcpy( debugText, filename );
	if ( isOK == true )
	{
		strcat( debugText, " loaded\n" );
		
	}
	else
	{
		strcat( debugText, " failed !!!\n" );
	}
	pspDebugScreenPrintf( debugText );
	if ( isOK == false )
	{
		for ( ; ; );
	}
	
	return song;
}

// ---------------------------------------------------------------------------- PLAY SONG
void PlaySong( Song *song )
{
	StopSong( );
	
	Player_Start( ( UNIMOD * ) song->getData( ) );
	
	Player_SetPosition( 0 );
}

// ---------------------------------------------------------------------------- STOP SONG
void StopSong( )
{
	Player_Stop( );
}

// ---------------------------------------------------------------------------- CONSTRUCTOR
Song::Song( char *filename, bool &isOK )
{
	m_data = 0;
	
	SceIoStat fileStatus;
	if ( sceIoGetstat( filename, &fileStatus ) < 0 )
	{
		isOK = false;
		return;
	}
	
	UNIMOD *data = MikMod_LoadSong( filename, 128 );
	if ( data == 0 )
	{
		isOK = false;
		return;
	}
	
	data->loop = 1; // enable song looping
	
	m_data = ( void * ) data;
        
	isOK = true;
}

// ---------------------------------------------------------------------------- DESTRUCTOR
Song::~Song( )
{
	if ( m_data )
	{
		MikMod_FreeSong( ( UNIMOD * )  m_data );
	}
}
