/******************************************************************************
 * sound.h                                                                    *
 ******************************************************************************
 * Project      : Bunny Tetris                                                *
 * License      : BSD license (check http://www.pspdev.org)                   *
 * Created by   : Arnaud Storq (http://norecess.planet-d.net)                 *
 ******************************************************************************/
#ifndef _SOUND_H__
#define _SOUND_H__

// ---------------------------------------------------------------------------- SONG CLASS
class Song
{
// ---------------------------------------------------------------------------- MEMBERS
protected:
	void *m_data;
	
public:

// ---------------------------------------------------------------------------- METHODS
protected:

public:
	Song( char *filename, bool &isOK );
	~Song( );
	
	void *getData( ) { return m_data; };
};

// ---------------------------------------------------------------------------- SOUND SYSTEM
void InitSoundSystem( );
void DestroySoundSystem( );

// ---------------------------------------------------------------------------- SONG
Song *LoadSong( char *filename );
void PlaySong( Song *song );
void StopSong( );

#endif 
